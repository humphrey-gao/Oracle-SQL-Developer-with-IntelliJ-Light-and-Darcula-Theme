# Oracle-SQL-Developer-with-IntelliJ-Light-and-Darcula-Theme
Using Flat Look and Feel(a Swing look and feel that imitates IntelliJ Platform) to make Oracle SQL Developer more mordern

## 效果：

### 	Light:

![](images/screenshot1.png)

![](images/screenshot2.png)

Dark(Darcula):

![](images/screenshot3.png)

![](images/screenshot4.png)

## 如何做到的：

###     在Oracle SQL Developer安装目录下的sqldeveloper\lib目录中放入`flatlaf-<version>.jar`文件。

例如本人在C:\sqldeveloper-19.2.1.247.2212\sqldeveloper\lib文件夹下放置了 [flatlaf.jar](https://github.com/humphreygao/Oracle-SQL-Developer-with-IntelliJ-Theme/blob/master/jar/flatlaf.jar ) 文件。

### 	找到Oracle SQL Developer安装目录下的sqldeveloper\bin目录中找到`sqldeveloper.conf`文件

例如该文件在本人电脑上的路径为：C:\sqldeveloper-19.2.1.247.2212\sqldeveloper\bin\sqldeveloper.conf。

### 	在`sqldeveloper.conf`文件的末尾加上如下配置：

```properties
# this is manual setting for Look and Feel
AddJavaLibFile ../lib/flatlaf.jar
AddVMOption -Dswing.defaultlaf=com.formdev.flatlaf.FlatLightLaf
```

其中`-Dswing.defaultlaf`的值可以为`com.formdev.flatlaf.FlatLightLaf`、`com.formdev.flatlaf.FlatIntelliJLaf`、`com.formdev.flatlaf.FlatDarkLaf`和`com.formdev.flatlaf.FlatDarculaLaf`共四种，前两种为日间主题，后两种为夜间主题。

